package com.example.nouvelikomobien.geo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.nouvelikomobien.geo.R;
import com.example.nouvelikomobien.geo.responses.CommentTO;
import com.example.nouvelikomobien.geo.models.Comment;
import com.example.nouvelikomobien.geo.rest.Api;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by nouvelikomobien on 15/03/2018.
 */

public class CommentActivity extends AppCompatActivity {

    EditText edtComment;
    RatingBar ratingNote;
    Button btnSendComment;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finish);

        preferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        edtComment = findViewById(R.id.edtComment);
        ratingNote = findViewById(R.id.ratingNote);
        btnSendComment = findViewById(R.id.btnSendComment);

        btnSendComment.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                final ProgressDialog progressDialog = new ProgressDialog(CommentActivity.this);
                progressDialog.setCancelable(false); // set cancelable to false
                progressDialog.setMessage("Please Wait"); // set message
                progressDialog.show(); // show progress dialog
                int idCourse = preferences.getInt("idCourse", -1);
                String email = preferences.getString("email", "null");
                CommentTO comment = new CommentTO();
                comment.setContent(edtComment.getText().toString());
                comment.setEmail(email);
                comment.setIdCourse(idCourse);
                Api.getClient().comment(comment, new Callback<Comment>() {
                    @Override
                    public void success(Comment commentServer, Response response) {
                        progressDialog.dismiss();
                        Intent i;
                        Log.i("comment", commentServer.getContent());
                        Toast.makeText(CommentActivity.this, commentServer.getContent(), Toast.LENGTH_LONG);
                        i = new Intent(CommentActivity.this, GetListCoursesActivity.class);
                        startActivity(i);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(CommentActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        Log.i("Error", error.toString());
                        progressDialog.dismiss(); //dismiss progress dialog
                    }
                });
            }
        });
    }
}
