package com.example.nouvelikomobien.geo.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nouvelikomobien on 25/01/2018.
 */

public class Survey implements Parcelable {

    @SerializedName("id")
    int id;
    @SerializedName("label")
    private String label;
    @SerializedName("responses")
    List<Response> responses;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public void setResponse(List<Response> responses) {
        this.responses = responses;
    }

    public Survey() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.label);
        dest.writeTypedList(this.responses);
    }

    protected Survey(Parcel in) {
        this.id = in.readInt();
        this.label = in.readString();
        this.responses = in.createTypedArrayList(Response.CREATOR);
    }

    public static final Parcelable.Creator<Survey> CREATOR = new Parcelable.Creator<Survey>() {
        @Override
        public Survey createFromParcel(Parcel source) {
            return new Survey(source);
        }

        @Override
        public Survey[] newArray(int size) {
            return new Survey[size];
        }
    };
}
