package com.example.nouvelikomobien.geo.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nouvelikomobien on 25/01/2018.
 */

public class Comment implements Parcelable {
    @SerializedName("id")
    int id;
    @SerializedName("content")
    String content;
    @SerializedName("course")
    Course course;
    @SerializedName("author")
    private User author;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Comment() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.content);
        dest.writeParcelable(this.course, flags);
        dest.writeParcelable(this.author, flags);
    }

    protected Comment(Parcel in) {
        this.id = in.readInt();
        this.content = in.readString();
        this.course = in.readParcelable(Course.class.getClassLoader());
        this.author = in.readParcelable(User.class.getClassLoader());
    }

    public static final Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}
