package com.example.nouvelikomobien.geo.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nouvelikomobien on 25/01/2018.
 */

public class User implements Parcelable {

    @SerializedName("username")
    private String username;
    private String password;
    @SerializedName("email")
    private String email;
    @SerializedName("visitedCourses")
    private List<Course> visitedCourses;
    @SerializedName("visitedPaths")
    private List<Path> visitedPaths;

    @SerializedName("userRole")
    private String userRole;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public List<Course> getVisitedCourses() {
        return visitedCourses;
    }

    public void setVisitedCourses(List<Course> visitedCourses) {
        this.visitedCourses = visitedCourses;
    }

    public List<Path> getVisitedPaths() {
        return visitedPaths;
    }

    public void setVisitedPaths(List<Path> visitedPaths) {
        this.visitedPaths = visitedPaths;
    }

    public User(String username, String email, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public User(){
        
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.username);
        dest.writeString(this.password);
        dest.writeString(this.email);
        dest.writeTypedList(this.visitedCourses);
        dest.writeTypedList(this.visitedPaths);
        dest.writeString(this.userRole);
    }

    protected User(Parcel in) {
        this.username = in.readString();
        this.password = in.readString();
        this.email = in.readString();
        this.visitedCourses = in.createTypedArrayList(Course.CREATOR);
        this.visitedPaths = in.createTypedArrayList(Path.CREATOR);
        this.userRole = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
