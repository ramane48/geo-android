package com.example.nouvelikomobien.geo.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.maps.android.geometry.Point;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by nouvelikomobien on 25/01/2018.
 */

public class Path implements Parcelable {

    @SerializedName("id")
    int id;
    @SerializedName("gpsPosition")
    com.example.nouvelikomobien.geo.models.Point from;
    @SerializedName("index")
    int index;
    @SerializedName("beginning")
    boolean isBeginning;
    @SerializedName("finish")
    boolean isFinish;
    @SerializedName("surveys")
    List<Survey> surveys;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Point getFrom() {
        return from;
    }

    public void setFrom(com.example.nouvelikomobien.geo.models.Point from) {
        this.from = from;
    }

    public List<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(List<Survey> surveys) {
        this.surveys = surveys;
    }

    public boolean isBeginning() {
        return isBeginning;
    }

    public void setBeginning(boolean beginning) {
        isBeginning = beginning;
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    public Path() {
    }

    public int getIndex() {
        return index;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeParcelable(this.from, flags);
        dest.writeInt(this.index);
        dest.writeByte(this.isBeginning ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isFinish ? (byte) 1 : (byte) 0);
        dest.writeList(this.surveys);
    }

    protected Path(Parcel in) {
        this.id = in.readInt();
        this.from = in.readParcelable(Point.class.getClassLoader());
        this.index = in.readInt();
        this.isBeginning = in.readByte() != 0;
        this.isFinish = in.readByte() != 0;
        this.surveys = new ArrayList<>();
        in.readList(this.surveys, Survey.class.getClassLoader());
    }

    public static final Parcelable.Creator<Path> CREATOR = new Parcelable.Creator<Path>() {
        @Override
        public Path createFromParcel(Parcel source) {
            return new Path(source);
        }

        @Override
        public Path[] newArray(int size) {
            return new Path[size];
        }
    };

    public static Comparator<Path> pathComparator = new Comparator<Path>() {
        public int compare(Path p1, Path p2) {
            int pathIndex1 = p1.getIndex();
            int pathIndex2 = p2.getIndex();

            //ascending order
            return pathIndex1 - pathIndex2;
        }
    };
}
