package com.example.nouvelikomobien.geo.rest;

/**
 * Created by nouvelikomobien on 15/02/2018.
 */

import com.example.nouvelikomobien.geo.responses.CommentTO;
import com.example.nouvelikomobien.geo.models.Comment;
import com.example.nouvelikomobien.geo.models.Course;
import com.example.nouvelikomobien.geo.responses.SignUpResponse;
import com.example.nouvelikomobien.geo.models.User;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

public interface ApiInterface {

    @POST("/users")     // API's endpoints
    public void registration(@Body User user,
                             Callback<SignUpResponse> callback);

    @POST("/login")
    public void login(@Body User user,
                      Callback<SignUpResponse> callback);

    /**
     * @param callback
     */
    @GET("/courses/")
    public void getCourseList(
            Callback<List<Course>> callback);

    /**
     * @param id
     * @param callback
     */
    @GET("/courses/{id}")
    void getCourse(@Path("id") int id,
                   Callback<Course> callback);

    @GET("/users")
    void getUserList(
            Callback<List<User>> callback);

    /**
     * @param id
     * @param callback
     */
    @GET("/users/{email}")
    void getUser(@Path("email") String id,
                   Callback<User> callback);

    @PUT("/paths/{email}/isvisitedpaths/{idPath}")
    void isvisitedPath(@Path("email") String email, @Path("idPath") int id, Callback<Boolean> callback);


    @PUT("/paths/{email}/visitpathby/{idPath}")
    void visitePath(@Path("email") String email, @Path("idPath") int id, Callback<User> callback);

    @PUT("/users/{email}/play/{idCourse}")
    void play(@Path("email") String email, @Path("idCourse") int id, Callback<User> callback);

    @POST("/courses/commentcourse")
    void comment(@Body CommentTO comment, Callback<Comment> callback);
}

