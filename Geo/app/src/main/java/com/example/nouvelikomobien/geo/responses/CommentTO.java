package com.example.nouvelikomobien.geo.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nouvelikomobien on 15/03/2018.
 */

/**
Classe pour envoyer un request du comment
 */
public class CommentTO {
    @SerializedName("content")
    private String content;
    @SerializedName("authorEmail")
    private String authorEmail;
    @SerializedName("courseId")
    private int idCourse;

    public CommentTO() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmail() {
        return authorEmail;
    }

    public void setEmail(String email) {
        this.authorEmail = email;
    }

    public int getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(int idCourse) {
        this.idCourse = idCourse;
    }
}
