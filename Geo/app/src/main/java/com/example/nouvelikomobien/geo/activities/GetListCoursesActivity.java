package com.example.nouvelikomobien.geo.activities;

/**
 * Created by nouvelikomobien on 15/02/2018.
 */


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.nouvelikomobien.geo.adapters.CoursesAdapter;
import com.example.nouvelikomobien.geo.models.Course;
import com.example.nouvelikomobien.geo.rest.Api;
import com.example.nouvelikomobien.geo.R;

import java.util.Collections;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GetListCoursesActivity extends AppCompatActivity {

    final Context context = this;
    RecyclerView recyclerView;
    List<Course> courseListResponseData;
    Button btnShowCustomDialog;
    private RadioGroup radioGroup;
    String filters = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        btnShowCustomDialog = findViewById(R.id.btnShowCustomDialog);
        setTitle("Liste des parcours");
        btnShowCustomDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_dialog);
                dialog.setCancelable(true);
                dialog.setTitle("Filters");
                Button dialogButton = (Button) dialog.findViewById(R.id.btn_filter);
                radioGroup = dialog.findViewById(R.id.radioGroupDialog);


                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        RadioButton TheTextIsHere;
                        TheTextIsHere = findViewById(radioGroup.getCheckedRadioButtonId());
                        if (null != TheTextIsHere)
                            filters = TheTextIsHere.getText().toString();
                    }
                });

                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        getCourseListData(filters); // call a method in which we have implement our GET type web API
    }

    private void getCourseListData(final String filters) {
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(GetListCoursesActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Please Wait"); // set message
        progressDialog.show(); // show progress dialog

        // Api is a class in which we define a method getClient() that returns the API Interface class object
        // getCoursesList() is a method in API Interface class, in this method we define our API sub url
        Api.getClient().getCourseList(new Callback<List<Course>>() {


            @Override
            public void success(List<Course> courses, Response response) {
                progressDialog.dismiss(); //dismiss progress dialog
                switch (filters) {
                    case "name":
                        Collections.sort(courses, Course.courseNameComparator);
                    case "note":
                        Collections.sort(courses, Course.courseNoteomparator);
                    case "duree":
                        Collections.sort(courses, Course.courseDurationComparator);
                        //case "Diff" : ;
                }
                courseListResponseData = courses;
                setDataInRecyclerView(); // call this method to set the data in adapter
            }

            @Override
            public void failure(RetrofitError error) {
                // if error occurs in network transaction then we can get the error in this method.
                Toast.makeText(GetListCoursesActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                Log.i("Error", error.toString());
                progressDialog.dismiss(); //dismiss progress dialog

            }
        });
    }

    private void setDataInRecyclerView() {
        // set a LinearLayoutManager with default vertical orientation
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(GetListCoursesActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        // call the constructor of CoursesAdapter to send the reference and data to Adapter
        CoursesAdapter coursesAdapter = new CoursesAdapter(GetListCoursesActivity.this, courseListResponseData);
        recyclerView.setAdapter(coursesAdapter); // set the Adapter to RecyclerView
    }
}
