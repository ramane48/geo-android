package com.example.nouvelikomobien.geo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nouvelikomobien.geo.R;
import com.example.nouvelikomobien.geo.models.Response;
import com.example.nouvelikomobien.geo.models.Survey;
import com.example.nouvelikomobien.geo.models.User;
import com.example.nouvelikomobien.geo.rest.Api;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by nouvelikomobien on 01/03/2018.
 */

public class FirstQuizActivity extends AppCompatActivity {
    private RadioGroup radioGroup;
    private Button btnNext;
    private TextView txtQuestion;
    List<Survey> surveys;
    List<Response> responses;
    int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz);

        responses = new ArrayList<>();
        surveys = new ArrayList<>();

        radioGroup = findViewById(R.id.radioGroup);
        txtQuestion = findViewById(R.id.txtQuestion);

        Intent intent = getIntent();
        final Bundle extras = intent.getExtras();

        surveys = extras.getParcelableArrayList("surveys");
        Log.i("Surveys", surveys.get(0).getResponses().get(0).getLabel());
        responses = surveys.get(0).getResponses();
        Toast.makeText(FirstQuizActivity.this, surveys.get(0).getLabel(), Toast.LENGTH_LONG).show();


        txtQuestion.setText(surveys.get(0).getLabel());

        addRadioButtons();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
                int idPath;
                boolean correct = false;

                for (Response response : responses) {
                    if (response.getId() == radioButton.getId() && response.isTrue()) {
                        correct = true;
                        SharedPreferences preferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

                        String email = preferences.getString("email", "");

                        idPath = (int) extras.get("idPath");
                        final ProgressDialog progressDialog = new ProgressDialog(FirstQuizActivity.this);
                        progressDialog.setCancelable(false); // set cancelable to false
                        progressDialog.setMessage("Please Wait"); // set message
                        progressDialog.show(); // show progress dialog
                        Callback callback = new Callback<User>() {

                            @Override
                            public void success(final User userListResponse, retrofit.client.Response response) {
                                progressDialog.dismiss();
                                setContentView(R.layout.correct_answer);
                                btnNext = findViewById(R.id.btnPass);
                                btnNext.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent i;
                                        if (extras.getString("finish").equals("true")) {
                                            setContentView(R.layout.finish);
                                        } else {
                                            Log.i("user", userListResponse.getEmail());
                                            //extras.putParcelable("user", userListResponse);
                                            i = new Intent(FirstQuizActivity.this, MapActivity.class).putExtras(extras);

                                            startActivity(i);
                                        }
                                    }
                                });
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                progressDialog.dismiss();
                                Toast.makeText(FirstQuizActivity.this, "Error", Toast.LENGTH_LONG);
                            }
                        };
                        Api.getClient().visitePath(email, idPath, callback);

                    }

                }
                if (!correct)
                setContentView(R.layout.wrong_answer);


            }
        });
    }

    public void addRadioButtons() {
        if (responses.size() > 0) {
            for (Response response : responses) {
                RadioButton rdbtn = new RadioButton(this);
                rdbtn.setId(response.getId());
                rdbtn.setText(response.getLabel());
                radioGroup.addView(rdbtn);
            }
        }

    }
}
