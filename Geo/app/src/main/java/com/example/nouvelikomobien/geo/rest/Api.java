package com.example.nouvelikomobien.geo.rest;

import retrofit.RestAdapter;

/**
 * Created by nouvelikomobien on 15/02/2018.
 */

public class Api {

    public static ApiInterface getClient() {

        // change your base URL
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint("https://ssh-paltine.herokuapp.com/api/rest/model") //Set the Root URL
                .build(); //Finally building the adapter

        //Creating object for our interface
        ApiInterface api = adapter.create(ApiInterface.class);

        return api; // return the APIInterface object


    }
}
