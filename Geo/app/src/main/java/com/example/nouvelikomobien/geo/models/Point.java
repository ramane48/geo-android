package com.example.nouvelikomobien.geo.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nouvelikomobien on 12/03/2018.
 */

public class Point extends com.google.maps.android.geometry.Point implements Parcelable {

    public Point(double x, double y) {
        super(x, y);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected Point(Parcel in) {
        super(in.readDouble(),in.readDouble());
    }

    public static final Parcelable.Creator<Point> CREATOR = new Parcelable.Creator<Point>() {
        @Override
        public Point createFromParcel(Parcel source) {
            return new Point(source);
        }

        @Override
        public Point[] newArray(int size) {
            return new Point[size];
        }
    };
}
