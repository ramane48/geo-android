package com.example.nouvelikomobien.geo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.nouvelikomobien.geo.R;

/**
 * Created by nouvelikomobien on 01/03/2018.
 */

public class QuizActivity extends AppCompatActivity {
    Button btn;
    TextView txt, txtEtape;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_home);
        btn = (Button) findViewById(R.id.button);
        txt = findViewById(R.id.txt);
        txtEtape = findViewById(R.id.txtEtape);

        Intent intent = getIntent();
        final Bundle extras = intent.getExtras();
        txtEtape.setText("Vous êtes à la " + String.valueOf(Integer.parseInt(extras.getString("index")) + 1) + " étape");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i;
                i = new Intent(QuizActivity.this, FirstQuizActivity.class).putExtras(extras);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}





