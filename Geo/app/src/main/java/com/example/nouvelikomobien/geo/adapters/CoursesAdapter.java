package com.example.nouvelikomobien.geo.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nouvelikomobien.geo.R;
import com.example.nouvelikomobien.geo.activities.LoginActivity;
import com.example.nouvelikomobien.geo.activities.MapActivity;
import com.example.nouvelikomobien.geo.models.Course;
import com.example.nouvelikomobien.geo.models.User;
import com.example.nouvelikomobien.geo.rest.Api;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by nouvelikomobien on 27/02/2018.
 */

public class CoursesAdapter extends RecyclerView.Adapter<CoursesAdapter.CourseViewHolder> {

    Context context;
    List<Course> coursesData;
    SharedPreferences preferences;

    public CoursesAdapter(Context context, List<Course> coursesData) {
        this.context = context;
        this.coursesData = coursesData;
    }

    @Override
    public CoursesAdapter.CourseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.courses_list_items, null);
        CoursesAdapter.CourseViewHolder coursesViewHolder = new CoursesAdapter.CourseViewHolder(view);
        return coursesViewHolder;
    }

    @Override
    public void onBindViewHolder(CourseViewHolder holder, int position) {
// set the data
        if (getItemCount() > 0) {
            final int pos = position;
            holder.name.setText("Name: " + coursesData.get(position).getName());
            holder.duration.setText("Durée: " + coursesData.get(position).getDuration());
            holder.creator.setText("Auteur: " + coursesData.get(position).getCourseCreator().getUsername());
            holder.note.setText("Note: " + coursesData.get(position).getNote());
            holder.description.setText("Description: " + coursesData.get(position).getDescription());
            //holder.creationDate.setText("Description: " + coursesData.get(position).getCreationDate());

            // implement setONCLickListtener on itemView
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // display a toast with course name
                    preferences = context.getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);
                    final SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("idCourse", coursesData.get(pos).getId());
                    editor.commit();
                    Api.getClient().play(preferences.getString("email", "null"), coursesData.get(pos).getId(), new Callback<User>() {
                        @Override
                        public void success(User user, Response response) {
                            Intent i = new Intent(context, MapActivity.class).putExtra("name", coursesData.get(pos).getName());
                            Bundle extras = new Bundle();
                            extras.putString("name", coursesData.get(pos).getName());
                            extras.putString("duration", String.valueOf(coursesData.get(pos).getDuration()));
                            extras.putString("id", String.valueOf(coursesData.get(pos).getId()));
                            i.putExtras(extras);
                            context.startActivity(i);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Toast.makeText(context, "Error", Toast.LENGTH_LONG);
                        }
                    });


                }
            });
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Ops il y a 0 parcours, vous pouvez créer sur votre profil sur la version web");
        }

    }

    @Override
    public int getItemCount() {
        return coursesData.size();
    }

    public class CourseViewHolder extends RecyclerView.ViewHolder {

        TextView name, duration, creator, note, description, creationDate;

        public CourseViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            duration = itemView.findViewById(R.id.duration);
            creator = itemView.findViewById(R.id.creator);
            note = itemView.findViewById(R.id.note);
            description = itemView.findViewById(R.id.description);
            creationDate = itemView.findViewById(R.id.creationDate);

        }
    }
}
