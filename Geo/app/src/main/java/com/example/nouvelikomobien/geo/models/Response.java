package com.example.nouvelikomobien.geo.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nouvelikomobien on 10/03/2018.
 */

public class Response implements Parcelable {

    @SerializedName("id")
    int id;
    @SerializedName("label")
    private String label;
    @SerializedName("true")
    private boolean isTrue;

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isTrue() {
        return isTrue;
    }

    public void setTrue(boolean True) {
        isTrue = True;
    }

    public Response() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.label);
        dest.writeByte(this.isTrue ? (byte) 1 : (byte) 0);
    }

    protected Response(Parcel in) {
        this.id = in.readInt();
        this.label = in.readString();
        this.isTrue = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Response> CREATOR = new Parcelable.Creator<Response>() {
        @Override
        public Response createFromParcel(Parcel source) {
            return new Response(source);
        }

        @Override
        public Response[] newArray(int size) {
            return new Response[size];
        }
    };
}
