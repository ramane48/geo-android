package com.example.nouvelikomobien.geo.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by nouvelikomobien on 25/01/2018.
 */

public class Course implements Parcelable {

    @SerializedName("id")
    int id;
    @SerializedName("name")
    String name;
    @SerializedName("duration")
    double duration;
    @SerializedName("note")
    double note;
    @SerializedName("comments")
    List<Comment> comment;
    @SerializedName("courseCreator")
    User courseCreator;
    @SerializedName("paths")
    List<Path> paths;
    @SerializedName("creationDate")
    private String creationDate;
    @SerializedName("description")
    String description;
    @SerializedName("difficulty")
    private int difficulty;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    public List<Comment> getComment() {
        return comment;
    }

    public void setComment(List<Comment> comment) {
        this.comment = comment;
    }

    public User getCourseCreator() {
        return courseCreator;
    }

    public void setCourseCreator(User courseCreator) {
        this.courseCreator = courseCreator;
    }

    public List<Path> getPaths() {
        return paths;
    }

    public void setPaths(List<Path> paths) {
        this.paths = paths;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public Course() {
    }


    public static Comparator<Course> courseNameComparator = new Comparator<Course>() {
        public int compare(Course c1, Course c2) {
            String courseName1 = c1.getName().toUpperCase();
            String courseName2 = c2.getName().toUpperCase();

            //ascending order
            return courseName1.compareTo(courseName2);
        }
    };

    public static Comparator<Course> courseDurationComparator = new Comparator<Course>() {
        public int compare(Course c1, Course c2) {
            int courseDuration1 = (int) c1.getDuration();
            int courseDuration2 = (int) c2.getDuration();

            //ascending order
            return courseDuration1 - courseDuration2;
        }
    };

    public static Comparator<Course> courseNoteomparator = new Comparator<Course>() {
        public int compare(Course c1, Course c2) {
            int courseNote1 = (int) c1.getNote();
            int courseNote2 = (int) c2.getNote();

            //ascending order
            return courseNote1 - courseNote2;
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeDouble(this.duration);
        dest.writeDouble(this.note);
        dest.writeTypedList(this.comment);
        dest.writeParcelable(this.courseCreator, flags);
        dest.writeTypedList(this.paths);
        dest.writeString(this.creationDate);
        dest.writeString(this.description);
        dest.writeInt(this.difficulty);
    }

    protected Course(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.duration = in.readDouble();
        this.note = in.readDouble();
        this.comment = in.createTypedArrayList(Comment.CREATOR);
        this.courseCreator = in.readParcelable(User.class.getClassLoader());
        this.paths = in.createTypedArrayList(Path.CREATOR);
        this.creationDate = in.readString();
        this.description = in.readString();
        this.difficulty = in.readInt();
    }

    public static final Parcelable.Creator<Course> CREATOR = new Parcelable.Creator<Course>() {
        @Override
        public Course createFromParcel(Parcel source) {
            return new Course(source);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };
}
