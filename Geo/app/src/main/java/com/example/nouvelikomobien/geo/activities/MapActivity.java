package com.example.nouvelikomobien.geo.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import android.location.Location;
import android.os.Looper;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


import org.json.JSONObject;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import android.widget.TextView;
import android.widget.Toast;

import com.example.nouvelikomobien.geo.R;
import com.example.nouvelikomobien.geo.models.Course;
import com.example.nouvelikomobien.geo.models.Path;
import com.example.nouvelikomobien.geo.models.User;
import com.example.nouvelikomobien.geo.rest.Api;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

/**
 * Created by nouvelikomobien on 02/03/2018.
 */

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {
    GoogleMap map;
    ArrayList<LatLng> markerPoints;

    boolean begin = false;
    Bundle extras;
    private static final int LOCATION_REQUEST = 500;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */

    SharedPreferences preferences;


    TextView txtCourseName, txtCourseDuration;
    List<Path> paths = new ArrayList<>();
    List<Path> visitedPaths = new ArrayList<>();

    private LocationRequest mLocationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_waypoint);
        preferences = getSharedPreferences(LoginActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        extras = getIntent().getExtras();

        String courseName = !(null == extras) ? extras.getString("name") : "";
        String courseDuration = !(null == extras) ? extras.getString("duration") : "";
        String courseId = !(null == extras) ? extras.getString("id") : "1";
        int id = Integer.parseInt(courseId);
        Log.i("id", String.valueOf((id)));

        setTitle(courseName);


        // Initializing
        markerPoints = new ArrayList<>();

        txtCourseName = findViewById(R.id.txtCourseName);
        txtCourseDuration = findViewById(R.id.txtCourseDuration);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        txtCourseName.setText(courseName);
        txtCourseDuration.setText(courseDuration);

       // this.startLocationUpdates();

        Api.getClient().getUser(preferences.getString("email", "null"), new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                visitedPaths = user.getVisitedPaths();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MapActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        this.startLocationUpdates();

    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Waypoints
        String waypoints = "";
        for (int i = 2; i < markerPoints.size(); i++) {
            LatLng point = (LatLng) markerPoints.get(i);
            if (i == 2)
                waypoints = "waypoints=";
            waypoints += point.latitude + "," + point.longitude + "|";
        }


        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * Initialiser la map et mettre les différents
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        map.getUiSettings().setZoomControlsEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            return;
        }
        map.setMyLocationEnabled(true);

        String courseId = !(null == extras) ? extras.getString("id") : "1";
        int id = Integer.parseInt(courseId);


        Api.getClient().getCourse(id, new Callback<Course>() {

            @Override
            public void success(Course course, Response response) {

                paths = course.getPaths();

                Collections.sort(paths, Path.pathComparator);

                if (paths.size() > 0) {

                    for (Path path : paths) {
                        markerPoints.add(new LatLng(path.getFrom().x, path.getFrom().y));

                        // Creating MarkerOptions
                        MarkerOptions options = new MarkerOptions();

                        // Setting the position of the marker
                        options.position(new LatLng(path.getFrom().x, path.getFrom().y));

                        /**
                         * For the start location, the color of marker is GREEN and
                         * for the end location, the color of marker is RED and
                         * for the rest of markers, the color is AZURE
                         */
                        if (path.isBeginning()) {
                            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)).title("Debut");
                        } else if (path.isFinish()) {
                            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).title("Fin");
                        } else if (isVisited(visitedPaths, path)) {
                            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)).title(String.valueOf(path.getIndex() + " Visitée"));

                        } else {
                            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).title(String.valueOf(path.getIndex()));
                        }

                        // Add new marker to the Google Map Android API V2
                        map.addMarker(options);

                    }
                }

                LatLng origin = null;
                LatLng dest = null;

                for (Path path : paths) {
                    if (paths.size() >= 2) {
                        if (path.isBeginning()) {
                            origin = new LatLng(path.getFrom().x, path.getFrom().y);
                        } else if (path.isFinish()) {
                            dest = new LatLng(path.getFrom().x, path.getFrom().y);
                        }
                    }
                }
                // Getting URL to the Google Directions API
                String url = getDirectionsUrl(origin, dest);

                DownloadTask downloadTask = new DownloadTask();

                // Start downloading json data from Google Directions API
                downloadTask.execute(url);
            }

            @Override
            public void failure(RetrofitError error) {
                // if error occurs in network transaction then we can get the error in this method.
                Toast.makeText(MapActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                Log.i("Error", error.toString());
            }
        });

    }


    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points = new ArrayList<>();
            PolylineOptions lineOptions = new PolylineOptions();
            if (null == result) return;
            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(7);
                lineOptions.color(Color.RED);
            }

            // Drawing polyline in the Google Map for the i-th route
            map.addPolyline(lineOptions);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    map.setMyLocationEnabled(true);
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    // Trigger new location updates at interval
    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        // do work here
                        onLocationChanged(locationResult.getLastLocation());
                    }
                },
                Looper.myLooper());
    }
    /**
     * Fonction qui se déclenche quand la position change et
     * vérifie si le joueur est arrivé à une étape
     */

    public void onLocationChanged(Location location) {
        // New location has now been determined
        float[] distance = new float[1];
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        for (Path pathP : paths) {
            boolean  visited = false;
            Location.distanceBetween(pathP.getFrom().x, pathP.getFrom().y, location.getLatitude(), location.getLongitude(), distance);
            // distance[0] is now the distance between these lat/lons in meters
            if (distance[0] < 5.0) {

                if (visitedPaths.isEmpty() && !pathP.isBeginning()) {
                    builder.setMessage("Ops il faut aller à l'étape de début");
                    begin = true;
                    builder.setCancelable(true);
                    builder.show();
                    break;
                }

                if (isVisited(visitedPaths, pathP)) {
                    visited = true;
                    builder.setMessage("Ops vous êtiez déjà là, il faut aller à l'étape suivante");
                    builder.setCancelable(true);
                    builder.show();
                    break;
                }


                if (pathP.isFinish())
                    extras.putString("finish", "true");
                else
                    extras.putString("finish", "");

                if (visited ||  begin) {

                }else{
                    extras.putString("index", String.valueOf(pathP.getIndex()));
                    extras.putInt("idPath", pathP.getId());
                    extras.putParcelableArrayList("surveys", (ArrayList<? extends Parcelable>) pathP.getSurveys());
                    Intent i = new Intent(this, FirstQuizActivity.class).putExtras(extras);
                    startActivity(i);
                }

            }
        }
    }

    /**
     * Fonction qui retourne true si le path en parametere
     * exist deja parmi les paths visités
     */
    private boolean isVisited(List<Path> visitedPaths, Path path2) {
        for (Path path : visitedPaths) {
            if (path.getId() == path2.getId()) {
                return true;
            }
        }
        return false;
    }
}
