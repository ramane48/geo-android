package com.example.nouvelikomobien.geo.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.nouvelikomobien.geo.R;
import com.example.nouvelikomobien.geo.models.User;
import com.example.nouvelikomobien.geo.responses.SignUpResponse;
import com.example.nouvelikomobien.geo.rest.Api;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by nouvelikomobien on 08/03/2018.
 */

public class LoginActivity extends AppCompatActivity {

    EditText email, password;
    Button login, signUp;
    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.login);
        signUp = findViewById(R.id.sign_in);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // validate the fields and call sign method to implement the api
                if (validate(email) && validate(password)) {
                    signUp();
                }
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // validate the fields and call sign method to implement the api
                Intent i;
                i = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(i);
            }
        });

    }

    private boolean validate(EditText editText) {
        // check the length of the enter data in EditText and give error if its empty
        if (editText.getText().toString().trim().length() > 0) {
            return true; // returns true if field is not empty
        }
        editText.setError("Please Fill This");
        editText.requestFocus();
        return false;
    }

    private void signUp() {
        // display a progress dialog
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setCancelable(false); // set cancelable to false
        progressDialog.setMessage("Please Wait"); // set message
        progressDialog.show(); // show progress dialog

        SharedPreferences pref = getApplicationContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE); // 0 - for private mode
        final SharedPreferences.Editor editor = pref.edit();

        // Api is a class in which we define a method getClient() that returns the API Interface class object
        // registration is a POST request type method in which we are sending our field's data

        final User user = new User();
        user.setEmail(email.getText().toString().trim());
        user.setPassword(password.getText().toString().trim());

        Api.getClient().login(user, new Callback<SignUpResponse>() {
            @Override
            public void success(SignUpResponse signUpResponse, Response response) {
                // in this method we will get the response from API
                progressDialog.dismiss(); //dismiss progress dialog

                Bundle extras = new Bundle();

                extras.putParcelable("user", user);

                editor.putString("email", email.getText().toString().trim()); // Storing string
                editor.commit();

                Intent i;
                i = new Intent(LoginActivity.this, GetListCoursesActivity.class).putExtras(extras);
                startActivity(i);
            }

            @Override
            public void failure(RetrofitError error) {
                // if error occurs in network transaction then we can get the error in this method.
                Toast.makeText(LoginActivity.this, "Login or password not correct", Toast.LENGTH_LONG).show();
                progressDialog.dismiss(); //dismiss progress dialog

            }
        });
    }
}
